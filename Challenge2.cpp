//Thanapat Sornsrivichai
//No.1027
//Challenge #2 (Connect 4)

#include<iostream>
using namespace std;
void display();
char playerswap(int);
int full(int);
char table[6][7];
void fill(int,char);
int findRow(int);
bool check(int,int);


//Main Function
int main()
{
	int coll_input,coll_hold,k=0,count=0,row_hold;
	char player;
	bool status = false;
	for(int r=0 ; r<6; r++)
	{
		for(int c=0 ; c<7 ; c++)
		{
			table[r][c] = ' ';
		}
	}
	display(); //Display first table for players
	cout << endl;
	while(status == false) //Loop until status = true (got a winner)
	{
		if(count!=42) //For the DRAW case
		{
			player=playerswap(k); //Swap player1 & 2 for each round
				if(player=='X')
				{
				cout <<"Player 1 (X) Turn!"<<endl;
				}
				else if(player=='O')
				{
				cout <<"Player 2 (O) Turn!"<<endl;	
				}
			while(true) //Get collum number
			{
			cin >> coll_input;
			coll_hold = coll_input - 1;
				if(full(coll_hold)==0)
				{
				cout << "This collum is full please change collum"	<<endl; //In case collum is Full
				}
					else if(coll_input>=1 && coll_input<=7)
					{
						break;
					}
					else cout << "Invalid number pls input number between 1-7" <<endl; //In case player input wrong number
			}
			fill(coll_hold,player);
			row_hold = findRow(coll_hold);
			status=check(row_hold,coll_hold);
			cout << endl;
			display();
			k++;
			count++;
		}
		else if(count>=42) //DRAW
		{
			cout <<endl<<endl<<"*****************************"<<endl<<">>>>>>   DRAW    <<<<<<";
			return 0;
		}
	}       //GOT A WINNER
		cout <<endl<<endl<<"*****************************"<<endl<<">>>>   Player ("<<player<<") WIN"<<"   <<<<"; 
		return 0;	
}

//Display the table
void display()
{
	cout<<" 1   2   3   4   5   6   7" <<endl;
	for(int a = 0; a<6 ; a++)
	{
		for(int b=0 ; b<7 ; b++)
		{
			cout << "|" << table[a][b] <<"|"<< ' ';
		}
		cout <<endl<<endl;
	}
}

//Swap player for each round
char playerswap(int k)
{
	char player1='X',player2='O';
	int mol;
	mol=k%2;
	if(mol==0)
	{
		return player1;
	}
	else return player2;
}

//In case the collum is full
int full(int coll)
{
	if(table[0][coll]!=' ')
	{
		return 0;
	}
}

//Use to fill in the box
void fill(int coll,char player)
{
	for(int i=0 ; i<6 ;i++)
	{
		if(table[5][coll] == ' ')
	{
		table[5][coll]=player;
		break;
	}
		
		if(table[i][coll] != ' ')
		{
			table[i-1][coll] = player;
			break;
		}
	}
}

//Finding what row it stop at (Use for checking)
int findRow(int coll)
{
	for(int i =0 ; i<6 ; i++)
	{
		if(table[i][coll] != ' ')
		{
			return i;
		}
	}
}

//Check for winner in each round
bool check(int r, int c)
{
	char player = table[r][c];
	int vertical = 1;
	int horizontal = 1;
	int diagonal_left = 1;
	int diagonal_right = 1;
	int row,coll;
	
	//Check Vertical
	for(row = r +1;row <= 5 && table[row][c] == player;row++)
	{
		vertical++;
	}
	for(row = r -1;table[row][c] == player && row >= 0;row--)
	{
		vertical++;
	}
		if(vertical >= 4)
		{
			return true;
		}
		
	//Check Horizontal
	for(coll = c -1;table[r][coll] == player && coll >= 0;coll--)
	{
		horizontal++;
	}
	for(coll = c +1;table[r][coll] == player && coll <= 6;coll++)
	{
		horizontal++;
	}
		if(horizontal >= 4)
		{
			return true;
		} 
		
	//Check Diagonal (Left)
	for(row = r -1, coll= c -1;table[row][coll] == player && row>=0 && coll >=0;row --, coll --)
	{
		diagonal_left ++;
	}
	for(row = r +1, coll= c +1;table[row][coll] == player && row<=5 && coll <=6;row ++, coll ++)
	{
		diagonal_left ++;
	}
		if(diagonal_left >= 4) 
		{
			return true;
		}
	
	//Check Diagonal (Right)
	for(row = r -1, coll= c +1;table[row][coll] == player && row>=0 && coll <= 6; row --, coll ++)
	{
		diagonal_right ++;
	}
	for(row = r +1, coll= c -1;table[row][coll] == player && row<=5 && coll >= 0; row ++, coll --)
	{
		diagonal_right ++;
	}
		if(diagonal_right >= 4) 
		{
			return true;
		}
		
	return false;
}
